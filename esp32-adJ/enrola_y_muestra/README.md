Programa para un esp32 que permite reconocer huella con un lector R-307 y enviar 
sus características por conexión serial al programa servirhuella que corre 
en el PC.  

Se ha probado con makeesparduino para compilar en adJ/OpenBSD (7.2). 

Agregue el usuario con el cual escribirá al grupo `dialer`.

Instale los paquetes requeridos:

	doas pkg_add makeesparduino \
		arduino \
		arduino-esp32 \
		arduino-makefile \
		py3-esptool \
		xtensa-esp32-elf-gcc \
		xtensa-esp32-elf-binutils \
		xtensa-esp32-elf-newlib \
		mkspiffs \
		gmake

Y después compile con

	gmake clean
	gmake

A continuación puede subirlo al microcontrolador con:
	
	gmake flash

