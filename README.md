# Servidor web mínimo para servir huella digital

A futuro servirá huella digital extrayendola de un lector de huellas

Por el momento sólo es servidor web que responde diferente en la ruta
`/v1/servirhuella`

A futuro queremos que en esa ruta retorne las características de la
huella digital tal como las emita un lector de huellas digitales por
elegir.


## Requerimientos

Librería microhttpd y sus encabezados.

En adJ/OpenBSD 7.2 se instala con:

    doas pkg_add libmicrohttpd

En Ubuntu 22.04 se instala con:

    sudo apt install libmicrohttpd12 libmicrohttpd-dev


## Compilación

En linux basta:

    make

## Uso

Tras compilar copie en el mismo directorio del ejecutable un certificado
TLS con llave publica en `servidor.pem` y con llave privad `servidor.key`.

Después ejecute el servidor con

    ./servirhuellas

Desde un navegador en el mismo computador emplee rutas de la forma

    https://localhost:8888/ruta_arbitraria

que deben responder con `Ruta invalida`

La ruta `/v1/servirhuella` por ahora responde con la misma ruta y
buscaremos que proximamente responda activando el lector de huellas
dactilares como para enrolar una huella, requiriendo que la persona
ponga su huella una o dos veces y después enviará las característica
de la huella al servidor y este a su vez retornará un JSON con 
esa información.
