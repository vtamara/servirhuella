/*
 * Copyright (c) 2023 Vladimir Támara Patiño <vtamara@pasosdeJesus.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* Empleando un microcontralador Arduino UNO conectado a un lector de
   huellas R-307.  Enrola huella y la envía al PC por puerto serial */

#include <Adafruit_Fingerprint.h>

#if (defined(__AVR__) || defined(ESP8266)) && !defined(__AVR_ATmega2560__)
// For UNO and others without hardware serial, we must use software serial...
// pin #2 is IN from sensor (GREEN wire)
// pin #3 is OUT from arduino  (WHITE wire)
// Set up the serial port to use softwareserial..
SoftwareSerial mySerial(2, 3);

#else
// On Leonardo/M0/etc, others with hardware serial, use hardware serial!
// #0 is green wire, #1 is white
#define mySerial Serial1

#endif


Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);

int getFingerprintIDez();

void setup()
{
  while (!Serial);
  Serial.begin(9600);  // Nos ha funcionado hasta 115200
  Serial.println("Enrola y envia características de huella");

  // set the data rate for the Arduino software serial port
  finger.begin(57600);
  
  // set baud rate for sensor to 57600
  //finger.setBaudRate(6);

  if (finger.verifyPassword()) {
    Serial.println("F!");
  } else {
    Serial.print("*"); Serial.println("PWD");
    while (1);
  }


}


void loop()
{
  int num = 0;
  while (num != 1) {
    while (! Serial.available());
    num = Serial.parseInt();
  }
  int coderr;
  coderr = getFingerprintEnroll(1);
  if (coderr == FINGERPRINT_OK) {
     coderr = downloadFingerprintTemplate(1);
  }
  //coderr = enviarImagenAPC();

  if (coderr != FINGERPRINT_OK) {
    Serial.print("*"); Serial.println(coderr);
  }
}

/* GET_CMD_PACKET y SEND_CMD_PACKET copiados  de librería de adafruit */
/*!
 * @brief Gets the command packet
 */
#define GET_CMD_PACKET(...)                                                    \
  uint8_t data[] = {__VA_ARGS__};                                              \
  Adafruit_Fingerprint_Packet packet(FINGERPRINT_COMMANDPACKET, sizeof(data),  \
                                     data);                                    \
  finger.writeStructuredPacket(packet);                                               \
  if (finger.getStructuredPacket(&packet) != FINGERPRINT_OK)                          \
    return FINGERPRINT_PACKETRECIEVEERR;                                       \
  if (packet.type != FINGERPRINT_ACKPACKET)                                    \
    return FINGERPRINT_PACKETRECIEVEERR;

/*!
 * @brief Sends the command packet
 */
#define SEND_CMD_PACKET(...)                                                   \
  GET_CMD_PACKET(__VA_ARGS__);                                                 \
  return packet.data[0];

/* La funcionalidad de pasar imagen de sensor a computador no está en la librería de adafruit 
  (que nos parece feo de adafruit)  pero si en manual de referencia del lector: 
   https://www.rhydolabz.com/documents/37/r307-fingerprint-module-user-manual.pdf 

   Formato de paquete de la orden:
   | 2 bytes | 4bytes         | 1 byte             | 2 bytes        | 1 byte            | 2 bytes  |
   | Header  | Module address | Package identifier | Package length | Instruction code  | Checksum |
   | 0xEF01  | Xxxx           | 01H                | 03H            | 0aH               | 000eH    |
   
   Formato de paquete de la respuesta:
   | 2 bytes | 4bytes         | 1 byte             | 2 bytes        |1 byte             | 2 bytes  |
   | Header  | Module address | Package identifier | Package length | Confirmation code | Checksum |
   | 0xEF01  | Xxxx           | 07H                | 03H            | xxH               | sum      |
   Note 1： Código de confirmación=00H: listo para transferir siguiente paquete de datos;
            Código de confirmación=00H: error al recibir paquete;
            Código de confirmation=0fH: fallo al transferir el siguiente paquete de datos;
        2 ： El módulo transferirá el siguiente paquete de datos tras responder al computador.
   */
#define FINGERPRINT_UPIMAGE 0x0A
uint8_t upImage(void) {
  SEND_CMD_PACKET(FINGERPRINT_UPIMAGE);
}


#define TIMAGEN 40032
// Determinada experimentando

#define TCOLCHON 1300

// Esta función hace el mejor esfuerzo por transmitir imagen de la huella al PC.
// Pero por los 2048 bytes de RAM de un Arduino UNO, de los 40032 bytes de la imagen de la huella
// este método lograría pasar al computador máximo 27334 que es como el 68%.
uint8_t enviarImagenAPC() {
  // TRANSMISIÓN DE LA IMAGEN DE LA HUELLA
  ////////////////////////////////////////

  Serial.println("dw");

  uint8_t p = upImage();
  switch (p) {
    case FINGERPRINT_OK:
       break;
    default:
      Serial.print("U*"); Serial.println(p);
      return p;
  }

  uint8_t bytesReceived[TCOLCHON];
  //memset(bytesReceived, 0xff, 534);
  
  uint32_t starttime = millis();
  uint32_t i = 0;  
  uint32_t ic = 0;
  while (i < TIMAGEN && (millis() - starttime) < 20000) {
    if (mySerial.available()) {
      uint8_t l = mySerial.read();
      bytesReceived[ic] = l;
      i++;
      ic++;
      if (ic == TCOLCHON) {
        Serial.write(bytesReceived, TCOLCHON);
        ic = 0;
      }
    }
    
  }
  Serial.println("**");  
  Serial.print(i); Serial.println(" bytes read of image.");
  
  
  return FINGERPRINT_OK;
}

uint8_t downloadFingerprintTemplate(uint16_t id)
{
  uint8_t p = finger.loadModel(id);
  switch (p) {
    case FINGERPRINT_OK:
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.print("*"); Serial.println(p);
      return p;
    default:
      Serial.print("*"); Serial.println(p);
      return p;
  }

  // OK success!

  p = finger.getModel();
  switch (p) {
    case FINGERPRINT_OK:
       break;
    default:
      Serial.print("*"); Serial.println(p);
      return p;
  }

  // one data packet is 267 bytes. in one data packet, 11 bytes are 'usesless' :D
  uint8_t bytesReceived[534]; // 2 data packets
  memset(bytesReceived, 0xff, 534);

  uint32_t starttime = millis();
  int i = 0;
  while (i < 534 && (millis() - starttime) < 20000) {
    if (mySerial.available()) {
      bytesReceived[i++] = mySerial.read();
    }
  }
  Serial.print(i); Serial.println(" bytes read.");
 
  uint8_t fingerTemplate[512]; // the real template
  memset(fingerTemplate, 0xff, 512);

  // filtering only the data packets
  int uindx = 9, index = 0;
  memcpy(fingerTemplate + index, bytesReceived + uindx, 256);   // first 256 bytes
  uindx += 256;       // skip data
  uindx += 2;         // skip checksum
  uindx += 9;         // skip next header
  index += 256;       // advance pointer
  memcpy(fingerTemplate + index, bytesReceived + uindx, 256);   // second 256 bytes

  for (int veces = 0 ; veces < 3; veces ++) {
    for (int i = 0; i < 512; ++i) {
      //Serial.print("0x");
      printHex(fingerTemplate[i], 2);
      //Serial.print(", ");
    }
    Serial.print("\nd!");
    Serial.println(veces);
  }

  
  return FINGERPRINT_OK;

  /*
    uint8_t templateBuffer[256];
    memset(templateBuffer, 0xff, 256);  //zero out template buffer
    int index=0;
    uint32_t starttime = millis();
    while ((index < 256) && ((millis() - starttime) < 1000))
    {
    if (mySerial.available())
    {
      templateBuffer[index] = mySerial.read();
      index++;
    }
    }

    Serial.print(index); Serial.println(" bytes read");

    //dump entire templateBuffer.  This prints out 16 lines of 16 bytes
    for (int count= 0; count < 16; count++)
    {
    for (int i = 0; i < 16; i++)
    {
      Serial.print("0x");
      Serial.print(templateBuffer[count*16+i], HEX);
      Serial.print(", ");
    }
    Serial.println();
    }*/
}



void printHex(int num, int precision) {
  char tmp[16];
  char format[128];

  sprintf(format, "%%.%dX", precision);

  sprintf(tmp, format, num);
  Serial.print(tmp);
}


// Retornar 0 si logra enrolar huella
// De lo contrario el código de error
uint8_t getFingerprintEnroll(int id) {

  int p = -1;
  Serial.print("W! #"); Serial.println(id);
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("I!");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.print("*"); Serial.println(p);
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.print("*"); Serial.println(p);
      break;
    default:
      Serial.print("*"); Serial.println(p);
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("I!");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.print("*"); Serial.println(p);
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.print("*"); Serial.println(p);
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.print("*"); Serial.println(p);
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.print("*"); Serial.println(p);
      return p;
    default:
      Serial.print("*"); Serial.println(p);
      return p;
  }

  Serial.println("R!");
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  Serial.print("ID "); Serial.println(id);
  p = -1;
  Serial.println("P!");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("I!");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.print("*"); Serial.println(p);
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.print("*"); Serial.println(p);
      break;
    default:
      Serial.print("*"); Serial.println(p);
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("I!");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.print("*"); Serial.println(p);
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.print("*"); Serial.println(p);
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.print("*"); Serial.println(p);
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.print("*"); Serial.println(p);
      return p;
    default:
      Serial.print("*"); Serial.println(p);
      return p;
  }

  // OK converted!
  Serial.print("Cm #");  Serial.println(id);

  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("P!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.print("*"); Serial.println(p);
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    Serial.print("*"); Serial.println(p);
    return p;
  } else {
    Serial.print("*"); Serial.println(p);
    return p;
  }

  Serial.print("ID "); Serial.println(id);
  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    Serial.println("S!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.print("*"); Serial.println(p);
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.print("*"); Serial.println(p);
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.print("*"); Serial.println(p);
    return p;
  } else {
    Serial.print("*"); Serial.println(p);
    return p;
  }

  return FINGERPRINT_OK;
}

