
/* Servidor web mínimo para enviar datos de huella dactilar
 *
 * Basado en tutorial de libmicrohttpd
 *
 * Licencia ISC. 2023. vtamara@pasosdeJesus.org
 **/

#include <dirent.h>
#include <fcntl.h>
#include <grp.h>
#include <langinfo.h>
#include <locale.h>
#include <pwd.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#ifndef _WIN32
#include <sys/select.h>
#include <sys/socket.h>
#else
#include <winsock2.h>
#endif

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include <microhttpd.h>

#include <SerialStream.h>

using namespace std;
using namespace LibSerial ;

#define PUERTO 8888

#define ARCHLLAVESERVIDOR "servidor.key"
#define ARCHCERTSERVIDOR "servidor.pem"

#define MHD_start_servicio MHD_start_daemon
#define MHD_stop_servicio MHD_stop_daemon

SerialStream serial_port ;

bool existe_archivo(const char *ruta) 
{
  struct stat colchon;
  int         estado;

  estado = stat(ruta, &colchon);

  return estado == 0;
}


static long
obtener_tamanio (const char *nomarch) 
{
  FILE *aparch;

  aparch = fopen (nomarch, "rb");
  if (aparch) {
    long tam;

    if ((0 != fseek (aparch, 0, SEEK_END)) || (-1 == (tam = ftell (aparch)))) {
      tam = 0;
    }

    fclose (aparch);

    return tam;
  }
  else {
    return 0;
  }
}


static char *
cargar_archivo (const char *nomarch)
{
  FILE *aparch;
  char *colchon;
  long tam;

  tam = obtener_tamanio (nomarch);
  if (0 == tam) {
    std::cerr << "Memoria insuficiente\n";
    return NULL;
  }

  aparch = fopen (nomarch, "rb");
  if (! aparch) {
    std::cerr << "No puede abrir " << nomarch << "\n";
    return NULL;
  }

  colchon = (char *)malloc (tam + 1);
  if (! colchon)
  {
    std::cerr << "Memoria insuficiente";
    fclose (aparch);
    return NULL;
  }
  colchon[tam] = '\0';

  if (tam != (long) fread (colchon, 1, tam, aparch))
  {
    std::cerr << "Lectura incompeta";
    free (colchon);
    colchon = NULL;
  }

  fclose (aparch);
  return colchon;
}

static int
responder_error(const char *merror, struct MHD_Connection *conexion)
{
  int ret;
  struct MHD_Response *respuesta;

  std::cerr << "Error: " << merror << "\n";

  respuesta =
    MHD_create_response_from_buffer(strlen (merror), (void *) merror,
        MHD_RESPMEM_MUST_COPY);

  MHD_add_response_header(respuesta, "Content-Type", "application/json");
  MHD_add_response_header(respuesta, "Access-Control-Allow-Origin", "*");

  ret = MHD_queue_response (conexion, MHD_HTTP_INTERNAL_SERVER_ERROR, respuesta);
  MHD_destroy_response (respuesta);

  return ret;
}



static int
responder_v1_servirhuella(const char *url, struct MHD_Connection *conexion)
{
  int ret;
  struct MHD_Response *respuesta;

  /* https://github.com/digi-embedded/linux/blob/v5.15/nxp/dey-4.0/maint/Documentation/usb/gadget_serial.rst
   * https://forum.arduino.cc/t/usb-read-write-between-arduino-and-ubuntu-pc/87080/5
   * */

  while( serial_port.rdbuf()->in_avail() == 0 ) {
    usleep(100) ;
  }

  usleep(10000);


  char out_buf[] = "1\n";
  serial_port.write(out_buf, 1);
  string busca;
  string huellas[3] = {"", "", ""};

  int veces = 0;
  int estado = 1; 
  // 1 Esperando "534 bytes read."
  // 2 Leyendo 1024 caracters de la  caracteristica de la huella
  // 3 Leyendo numero de iteración "\nd!#" con # siendo 0, 1 o 2
  while( 1 )
  {
    std::cerr << "estado=" << estado << endl;
    char next_byte;
    serial_port.get(next_byte);
    if (estado == 1) {
      busca.push_back(next_byte);
      if (busca.length() > 15)  {
        busca.erase(0, 1);
      }
      std::cerr << endl << "|" << busca << "|" << endl;
      if (busca.compare("534 bytes read.") == 0) {
        estado = 2;
        serial_port.get(next_byte); // \n
      } else if (busca.length() > 2 && busca.substr(busca.length() - 2, 1) == "*") {
        // Detección de error
        string merr = "Error código: " + busca.substr(busca.length() - 2, 2);
        return responder_error(merr.c_str(), conexion);
      }
    } else if (estado == 2) {
      if ((next_byte >= '0' && next_byte <= '9') || 
          (next_byte >= 'A' && next_byte <= 'F')) {
        huellas[veces].push_back(next_byte);
      } else {
        huellas[veces].push_back('X');
      }
      if (huellas[veces].length()>=1024) {
        busca = "";
        estado = 3;
        huellas[veces].erase(0, 1); // Quitamos \n inicial
        //std::cout << "Leimos características de huellas: " << huella << endl;
      }
    } else if (estado == 3) {
      busca.push_back(next_byte);
      if (busca.length() > 4) {
        busca.erase(0, 1);
      }
      if (busca.substr(0, 3).compare("\nd!") == 0) {
        estado = 2;
        int vleido = busca.c_str()[3] - '0';
        if (vleido == veces) {
          veces++;
          std::cerr << "veces = " << veces  << "\n";
          estado = 2;
          serial_port.get(next_byte); // \n
          busca = "";
          if (veces == 3) {
            estado = 0;
            break;
          }
        } else {
          string merr = "Se esperaba veces en " + to_string(veces) +
            " y se recibió " + to_string(vleido) + "\n";
          return responder_error(merr.c_str(), conexion);
        }
      }
    }
  }
  std::cerr << std::endl ;

  string cresp = "[\n";
  for(veces = 0; veces < 3; veces++) {
    cresp += "\"" + huellas[veces] + "\"";
    if (veces < 2) {
      cresp += ",";
    }
    cresp += "\n";
  }
  cresp += "]\n";

  respuesta = MHD_create_response_from_buffer(
      cresp.length(), (void *) cresp.c_str(), MHD_RESPMEM_MUST_COPY);
  if (! respuesta)
    return MHD_NO;
  MHD_add_response_header(respuesta, "Content-Type", "application/json");
  MHD_add_response_header(respuesta, "Access-Control-Allow-Origin", "*");

  ret = MHD_queue_response (conexion, MHD_HTTP_OK, respuesta);
  MHD_destroy_response (respuesta);

  return ret;
}


static int
responder_ruta_no_valida(const char *url, struct MHD_Connection *conexion)
{
  int ret;
  struct MHD_Response *respuesta;

  const char *pagina = "<html><body>Ruta no valida</body></html>";

  respuesta =
    MHD_create_response_from_buffer(strlen (pagina), (void *) pagina,
        MHD_RESPMEM_MUST_COPY);
  MHD_add_response_header(respuesta, "Access-Control-Allow-Origin", "*");

  ret = MHD_queue_response (conexion, MHD_HTTP_OK, respuesta);
  MHD_destroy_response (respuesta);

  return ret;
}



int
responder_a_conexion (
    void *cls, 
    struct MHD_Connection *conexion,
    const char *url, 
    const char *metodo,
    const char *version, 
    const char *upload_data,
    size_t *upload_data_tam, 
    void **con_cls)
{
  printf("url=%s", url);
  if (0 != strcmp (metodo, "GET")) {
    return MHD_NO;
  }
  if (NULL == *con_cls) {
    *con_cls = conexion;
    return MHD_YES;
  }
  if (strcmp(url, "/v1/servirhuella.json") == 0) {
    return responder_v1_servirhuella(url, conexion);
  } else {
    return responder_ruta_no_valida(url, conexion);
  }
}


int
main ()
{
  struct MHD_Daemon *servicio;
  char *llave_pem;
  char *cert_pem;

  std::cerr << "\nOJO*: servirhuella\n";
  char c;
  try {
    if (existe_archivo("/dev/ttyACM0")) {
      std::cerr << "OJO*: Existe /dev/ttyACM0\n";
      serial_port.Open( "/dev/ttyACM0" );
    } else if (existe_archivo("/dev/ttyACM1")) {
      std::cerr << "OJO*: Existe /dev/ttyACM1\n";
      serial_port.Open( "/dev/ttyACM1" );
    } else {
      std::cerr << "OJO*: No exite /dev/ttyACM{0,1}\n";
      exit(1);
    }
  } catch (int e) {
    std::cerr << "Excepción " << e << "\n";
    exit(1);
  }

   std::cerr << "Abrir\n";
  if ( ! serial_port.good() ) {
    std::cerr << "Error: No se pudo abrir puerto serial\n";
    exit(1);
  }
  std::cerr << "Tasa de baudios\n";
  serial_port.SetBaudRate( LibSerial::BaudRate::BAUD_9600 ) ;
  if ( ! serial_port.good() ) {
    std::cerr << "Error: No se pudo establecer tasa de baudios";
    exit(1);
  }
  std::cerr << "Número de bits\n";
  serial_port.SetCharacterSize( LibSerial::CharacterSize::CHAR_SIZE_8 ) ;
  if ( ! serial_port.good() ) {
    std::cerr << "Error: No se pudo establecer bits por carácter";
    exit(1);
  }
  std::cerr << "Sin paridad\n";
  serial_port.SetParity( LibSerial::Parity::PARITY_NONE ) ;
  if ( ! serial_port.good() ) {
    std::cerr << "Error: No se pudo deshabilitar paridad";
    exit(1);
  }
  std::cerr << "Bit de parada\n";
  serial_port.SetStopBits( LibSerial::StopBits::STOP_BITS_1 ) ;
  if ( ! serial_port.good() ) {
    std::cerr << "Error: No se pudo poner número de bits de parada";
    exit(1);
  }

  // Do not skip whitespace characters while reading from the
  // serial port.
  //
  // serial_port.unsetf( std::ios_base::skipws ) ;
  //
  // Wait for some data to be available at the serial port.
  //
  //
  // Keep reading data from serial port and print it to the screen.
  //
  // Wait for some data to be available at the serial port.
  //

  llave_pem = cargar_archivo (ARCHLLAVESERVIDOR);
  cert_pem = cargar_archivo (ARCHCERTSERVIDOR);

  if ((llave_pem == NULL) || (cert_pem == NULL))
  {
    printf ("No se pudieron leer llave/ceritificado.\n");
    if (NULL != llave_pem)
      free (llave_pem);
    if (NULL != cert_pem)
      free (cert_pem);
    return 1;
  
  
  }

  servicio =
    MHD_start_servicio(
        MHD_USE_INTERNAL_POLLING_THREAD, // | MHD_USE_TLS,  // unsigned int flags
        PUERTO, // unt16_t port
        NULL, // MHD_AcceptPolicyCallback apc
        NULL, // void *apc_cls
        &responder_a_conexion, //MDH_AccessHandlerCallback dh
        NULL,  // void *dh_cls
        MHD_OPTION_HTTPS_MEM_KEY,  //...
        llave_pem,
        MHD_OPTION_HTTPS_MEM_CERT, 
        cert_pem, 
        MHD_OPTION_END);
  if (NULL == servicio)
  {
    printf ("%s\n", cert_pem);

    free (llave_pem);
    free (cert_pem);

    return 1;
  }

  (void) getchar ();

  MHD_stop_servicio (servicio);
  free (llave_pem);
  free (cert_pem);

  return 0;
}
